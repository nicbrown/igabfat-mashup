<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>




<html lang="en-GB" hola_ext_inject="disabled"><!-- InstanceBegin template="/Templates/v4-hse-html5.dwt" codeOutsideHTMLIsLocked="true" --><!--<![endif]--><head>
<meta charset="utf-8">

<!-- v4.2.6 - html5 -->

<link rel="stylesheet" href="http://www.hse.gov.uk/assets/v4-css/core.min.css" type="text/css" media="screen,print">

<link rel="stylesheet" href="http://www.hse.gov.uk/assets/v4-css/website.min.css" type="text/css" media="screen,print">



	<c:url value="/resources/text.txt" var="url"/>
	<spring:url value="/resources/text.txt" htmlEscape="true" var="springUrl"/>
	<br>
	<h1>${message}</h1>
	<img data-width="469" data-height="200" alt="Star Wars Han Solo animated GIF" data-animated="https://media3.giphy.com/media/e3mqOpQjFEddS/200.gif" id="e3mqOpQjFEddS" src="https://media3.giphy.com/media/e3mqOpQjFEddS/200.gif" nopin="nopin" />



<script async="" src="//www.google.com/adsense/search/async-ads.js"></script><script type="text/javascript" async="" src="//uk1.siteimprove.com/js/siteanalyze_6798.js"></script><script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script><script src="assets/v4-js/site-tracker-web.min.js"></script>


<!-- InstanceBeginEditable name="doctitle" -->
<title>HSE: Information about health and safety at work</title>
<meta name="Keywords" content="HSE, health and safety executive,  occupational ill-health, occupational health and safety, accident, employee, health and safety at work act, law, guidance, slips, trips, falls, risk assessment, safety policy, shattered lives, legislation, statistics, science, research, enforcement, new business, starter pack, vehicles, piper alpha, schools, regulations, consultations, riddor, coshh">
<meta name="Description" content="Advice, guidance, news, templates, tools, legislation, publications from Great Britain's independent regulator for work-related health, safety and illness; HSE">

<meta name="robots" content="NOODP">
<link rel="stylesheet" href="http://www.hse.gov.uk/assets/v4-css/extra-homepage.min.css" type="text/css" media="screen,print">

<!--
<PageMap>
<DataObject type="thumbnail">
<Attribute name="src" value="http://www.hse.gov.uk/assets/images/logos/hse/logo-search.jpg"/>
<Attribute name="width" value="100"/>
<Attribute name="height" value="130"/>
</DataObject>
</PageMap>
--> 

<!-- InstanceEndEditable -->
<link rel="shortcut icon" href="favicon.ico">
<link rel="apple-touch-icon" href="http://www.hse.gov.uk/assets/v4-images/shared/apple-touch-icon.png">
<link rel="home" href="index.htm">
<link rel="alternate" type="application/rss+xml" title="What's new at HSE - NewsFeed" href="http://news.hse.gov.uk/feed/">

<script>
var _gaq= _gaq || [];
_gaq.push(['_setAccount', 'UA-324220-1']);
_gaq.push (['_gat._anonymizeIp']); 
_gaq.push(['_setDomainName','none']);
_gaq.push(['_trackPageview', document.location.pathname+document.location.search.toLowerCase()]);
(function() {
 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>


<style type="text/css">.gscb_a{display:inline-block;font:27px/13px arial,sans-serif}.gsst_a .gscb_a{color:#a1b9ed;cursor:pointer}.gsst_a:hover .gscb_a,.gsst_a:focus .gscb_a{color:#36c}.gsst_a{display:inline-block}.gsst_a{cursor:pointer;padding:0 4px}.gsst_a:hover{text-decoration:none!important}.gsst_b{font-size:16px;padding:0 2px;position:relative;user-select:none;-webkit-user-select:none;white-space:nowrap}.gsst_e{opacity:0.55;}.gsst_a:hover .gsst_e,.gsst_a:focus .gsst_e{opacity:0.72;}.gsst_a:active .gsst_e{opacity:1;}.gsst_f{background:white;text-align:left}.gsst_g{background-color:white;border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);margin:-1px -3px;padding:0 6px}.gsst_h{background-color:white;height:1px;margin-bottom:-1px;position:relative;top:-1px}.gsib_a{width:100%;padding:4px 6px 0}.gsib_a,.gsib_b{vertical-align:top}.gssb_c{border:0;position:absolute;z-index:989}.gssb_e{border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);cursor:default}.gssb_f{visibility:hidden;white-space:nowrap}.gssb_k{border:0;display:block;position:absolute;top:0;z-index:988}.gsdd_a{border:none!important}.gsq_a{padding:0}.gscsep_a{display:none}.gssb_a{padding:0 7px}.gssb_a,.gssb_a td{white-space:nowrap;overflow:hidden;line-height:22px}#gssb_b{font-size:11px;color:#36c;text-decoration:none}#gssb_b:hover{font-size:11px;color:#36c;text-decoration:underline}.gssb_g{text-align:center;padding:8px 0 7px;position:relative}.gssb_h{font-size:15px;height:28px;margin:0.2em;-webkit-appearance:button}.gssb_i{background:#eee}.gss_ifl{visibility:hidden;padding-left:5px}.gssb_i .gss_ifl{visibility:visible}a.gssb_j{font-size:13px;color:#36c;text-decoration:none;line-height:100%}a.gssb_j:hover{text-decoration:underline}.gssb_l{height:1px;background-color:#e5e5e5}.gssb_m{color:#000;background:#fff}.gsfe_a{border:1px solid #b9b9b9;border-top-color:#a0a0a0;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);}.gsfe_b{border:1px solid #4d90fe;outline:none;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);}.gssb_a{padding:0 7px}.gssb_e{border:0}.gssb_l{margin:5px 0}.gssb_c .gsc-completion-container{position:static}.gssb_c{z-index:5000}.gsc-completion-container table{background:transparent;font-size:inherit;font-family:inherit}.gssb_c > tbody > tr,.gssb_c > tbody > tr > td,.gssb_d,.gssb_d > tbody > tr,.gssb_d > tbody > tr > td,.gssb_e,.gssb_e > tbody > tr,.gssb_e > tbody > tr > td{padding:0;margin:0;border:0}.gssb_a table,.gssb_a table tr,.gssb_a table tr td{padding:0;margin:0;border:0}</style></head>
<body class="home normalFont hasScript" si:pageid="httpwwwhsegovuk">


<div id="top">

	<a href="#contentContainer" class="skip" id="tophttpwwwhsegovukcontentContainerSkiptocontent">Skip to content</a>


</div>

<div id="printBanner">
	<img src="http://www.hse.gov.uk/assets/v4-images/website/print/print-logo.jpg" width="193" height="65" alt="" id="printLogo">
	<img src="http://www.hse.gov.uk/assets/v4-images/website/print/print-header-bground.jpg" width="427" height="65" alt="" id="printBground">
</div>
<div id="cookieContainer" class="cf" style="display: none;">
	<div id="cookieNotify" class="cf">
    	<p>This website uses non-intrusive cookies to improve your user experience. You can visit our <a href="privacy-cookies.htm" id="cookieContainerhttpwwwhsegovukprivacycookieshtmcookieprivacypage">cookie privacy page</a> for more information.</p>
    <div id="cookieNotifyClose"><a href="#" id="cookieContainerhttpwwwhsegovuk"><img src="/assets/v4-images/shared/cookie-close.png" alt="Close this information"></a></div></div>
</div>
<div id="headerContainer" class="cf" role="banner">
	<div id="header" class="cf">



		<h1 id="headerLogo"><a href="index.htm" id="headerContainerhttpwwwhsegovukindexhtmHealthandSafetyExecutive">Health and Safety<br> Executive</a></h1>

	

			
		<div id="globalSearch">
			<div id="cse-search-form"><div class="gsc-control-searchbox-only gsc-control-searchbox-only-en" dir="ltr"><form class="gsc-search-box" accept-charset="utf-8"><table cellspacing="0" cellpadding="0" class="gsc-search-box"><tbody><tr><td class="gsc-input"><input autocomplete="off" type="text" size="10" class=" gsc-input" name="search" title="search" id="gsc-i-id1" dir="ltr" spellcheck="false" style="outline: none; background: url(http://www.google.com/cse/intl/en/images/google_custom_search_watermark.gif) 0% 50% no-repeat rgb(255, 255, 255);"><input type="hidden" name="bgresponse" id="bgresponse"></td><td class="gsc-search-button"><input type="button" value="Search" class="gsc-search-button" title="search"></td><td class="gsc-clear-button"><div class="gsc-clear-button" title="clear results">&nbsp;</div></td></tr></tbody></table><table cellspacing="0" cellpadding="0" class="gsc-branding"><tbody><tr><td class="gsc-branding-user-defined"></td><td class="gsc-branding-text"><div class="gsc-branding-text">powered by</div></td><td class="gsc-branding-img"><img src="http://www.google.com/uds/css/small-logo.png" class="gsc-branding-img"></td></tr></tbody></table></form></div></div>
        </div>
        
	</div>
	<div id="navigationContainer" class="cf" role="navigation" aria-label="Primary">    
		<ul class="navDropdown">
            <li class="home"><a href="index.htm" id="headerContainerhttpwwwhsegovukindexhtmHome">Home</a></li>
        	<li class="news"><a href="news/index.htm" id="headerContainerhttpwwwhsegovuknewsindexhtmNews">News</a><span class="arrow"></span><div class="navDrop navDropNews"><span class="navDropGap"></span><div class="navDropContent cf"><div class="navColLeft"> 
		<ul>
			<li><a href="http://press.hse.gov.uk/" id="headerContainerhttppresshsegovukMediacentre">Media centre</a></li>
			<li><a href="http://press.hse.gov.uk/release-type/press/" id="headerContainerhttppresshsegovukreleasetypepressPressreleases">Press releases</a></li>
			<li><a href="http://press.hse.gov.uk/regional/" id="headerContainerhttppresshsegovukregionalRegionalpressreleases">Regional press releases</a></li>
			<li><a href="http://press.hse.gov.uk/release-type/record/" id="headerContainerhttppresshsegovukreleasetyperecordPuttingtherecordstraight">Putting the record straight</a></li>		
		</ul>                       
	</div><div class="navColCentre"> 
		<ul>
			<li><a href="/news/subscribe/index.htm" id="headerContainerhttpwwwhsegovuknewssubscribeindexhtmSubscribetoeBulletins">Subscribe to eBulletins</a></li>
			<li><a href="/safetybulletins/index.htm" id="headerContainerhttpwwwhsegovuksafetybulletinsindexhtmSubscribetoHealthandsafetybulletins">Subscribe to Health and safety bulletins</a></li>
			<li><a href="http://news.hse.gov.uk/" id="headerContainerhttpnewshsegovukHSERSSFeeds">HSE RSS Feeds</a></li>
			<li><a href="http://news.hse.gov.uk/mobile/" id="headerContainerhttpnewshsegovukmobileHSERSSFeedsonmobiles">HSE RSS Feeds on mobiles</a></li>
		</ul>                          
	</div><div class="navColRight"> 
		<ul>
			<li><a href="/myth/index.htm" id="headerContainerhttpwwwhsegovukmythindexhtmHealthandsafetymyths">Health and safety myths</a></li>
			<li><a href="/news/judith-risk-assessment/index.htm" id="headerContainerhttpwwwhsegovuknewsjudithriskassessmentindexhtmJudithHackittsBlog">Judith Hackitt's Blog</a></li>
			<li><a href="/events/index.htm" id="headerContainerhttpwwwhsegovukeventsindexhtmEvents">Events</a></li>
		</ul>
	</div></div></div></li>
        <li class="guidance"><a href="guidance/index.htm" id="headerContainerhttpwwwhsegovukguidanceindexhtmGuidance">Guidance</a><span class="arrow"></span><div class="navDrop navDropGuidance"><span class="navDropGap"></span><div class="navDropContent cf"><div class="navColLeft"> 
		<div class="menu2Col">
        <h2>Topics</h2>
			<ul>
				<li><a href="/risk/index.htm" id="headerContainerhttpwwwhsegovukriskindexhtmRisk">Risk</a></li>
				<li><a href="/asbestos/index.htm" id="headerContainerhttpwwwhsegovukasbestosindexhtmAsbestos">Asbestos</a></li>
				<li><a href="/coshh/index.htm" id="headerContainerhttpwwwhsegovukcoshhindexhtmCOSHH"><abbr title="Control of Substances Hazardous to Health">COSHH</abbr></a></li>
				<li><a href="/stress/index.htm" id="headerContainerhttpwwwhsegovukstressindexhtmStress">Stress</a></li>
				<li><a href="/slips/index.htm" id="headerContainerhttpwwwhsegovukslipsindexhtmSlipsandtrips">Slips and trips</a></li>   
				<li><a href="/msd/index.htm" id="headerContainerhttpwwwhsegovukmsdindexhtmMSDs"><abbr title="Musculoskeletal disorders">MSDs</abbr></a></li>
				<li><a href="/temperature/index.htm" id="headerContainerhttpwwwhsegovuktemperatureindexhtmTemperature">Temperature</a></li>
				<li><a href="/work-at-height/index.htm" id="headerContainerhttpwwwhsegovukworkatheightindexhtmWorkatheight">Work at height</a></li> 
				<li><a href="/electricity/index.htm" id="headerContainerhttpwwwhsegovukelectricityindexhtmElectricity">Electricity</a></li>
				<li><a href="/comah/index.htm" id="headerContainerhttpwwwhsegovukcomahindexhtmCOMAH"><abbr title="Control of major accident hazards">COMAH</abbr></a></li>
			</ul>
			<ul>
				<li><a href="/workplacetransport/index.htm" id="headerContainerhttpwwwhsegovukworkplacetransportindexhtmWorkplacetransport">Workplace transport</a></li>
				<li><a href="/firstaid/index.htm" id="headerContainerhttpwwwhsegovukfirstaidindexhtmFirstaid">First aid</a></li>
				<li><a href="/noise/index.htm" id="headerContainerhttpwwwhsegovuknoiseindexhtmNoise">Noise</a></li>
				<li><a href="/gas/index.htm" id="headerContainerhttpwwwhsegovukgasindexhtmGas">Gas</a></li>
				<li><a href="/vibration/index.htm" id="headerContainerhttpwwwhsegovukvibrationindexhtmVibration">Vibration</a></li>   
				<li><a href="/youngpeople/index.htm" id="headerContainerhttpwwwhsegovukyoungpeopleindexhtmYoungpeople">Young people</a></li>
				<li><a href="/aboutus/occupational-disease/index.htm" id="headerContainerhttpwwwhsegovukaboutusoccupationaldiseaseindexhtmOccupationaldisease">Occupational disease</a></li>
				<li><a href="/work-equipment-machinery/index.htm" id="headerContainerhttpwwwhsegovukworkequipmentmachineryindexhtmWorkequipment">Work equipment</a></li>
				<li><a href="/reach/index.htm" id="headerContainerhttpwwwhsegovukreachindexhtmREACH"><abbr title="Registration, Evaluation, Authorisation &amp; restriction of CHemicals">REACH</abbr></a></li>
			</ul>
        	<p class="more"><a href="/guidance/topics.htm" id="headerContainerhttpwwwhsegovukguidancetopicshtmMoretopics">More topics...</a></p> 
		</div>                       
    </div><div class="navColCentre"> 
		<div class="menu2Col">
        	<h2>Industries</h2>
			<ul>
				<li><a href="/construction/index.htm" id="headerContainerhttpwwwhsegovukconstructionindexhtmConstruction">Construction</a></li>
				<li><a href="/offshore/index.htm" id="headerContainerhttpwwwhsegovukoffshoreindexhtmOffshore">Offshore</a></li>
				<li><a href="http://www.onr.org.uk/index.htm" id="headerContainerhttpwwwonrorgukindexhtmNuclear">Nuclear</a></li>
				<li><a href="/healthservices/index.htm" id="headerContainerhttpwwwhsegovukhealthservicesindexhtmHealthsocialcare">Health &amp; social care</a></li>   
				<li><a href="/agriculture/index.htm" id="headerContainerhttpwwwhsegovukagricultureindexhtmAgriculture">Agriculture</a></li>
				<li><a href="/catering/index.htm" id="headerContainerhttpwwwhsegovukcateringindexhtmCatering">Catering</a></li>              
				<li><a href="/biocides/index.htm" id="headerContainerhttpwwwhsegovukbiocidesindexhtmBiocides">Biocides</a></li>              
				<li><a href="/waste/index.htm" id="headerContainerhttpwwwhsegovukwasteindexhtmWasterecycling">Waste &amp; recycling</a></li>
				<li><a href="/woodworking/index.htm" id="headerContainerhttpwwwhsegovukwoodworkingindexhtmWoodworking">Woodworking</a></li>
				<li><a href="/food/index.htm" id="headerContainerhttpwwwhsegovukfoodindexhtmFood">Food</a></li>
			</ul>
			<ul>
				<li><a href="/services/education/index.htm" id="headerContainerhttpwwwhsegovukserviceseducationindexhtmEducation">Education</a></li>
				<li><a href="/mvr/index.htm" id="headerContainerhttpwwwhsegovukmvrindexhtmMotorvehiclerepair">Motor vehicle repair</a></li>
				<li><a href="/quarries/index.htm" id="headerContainerhttpwwwhsegovukquarriesindexhtmQuarries">Quarries</a></li>
				<li><a href="/chemicals/index.htm" id="headerContainerhttpwwwhsegovukchemicalsindexhtmChemicals">Chemicals</a></li>
				<li><a href="/explosives/index.htm" id="headerContainerhttpwwwhsegovukexplosivesindexhtmExplosives">Explosives</a></li>   
				<li><a href="/diving/index.htm" id="headerContainerhttpwwwhsegovukdivingindexhtmDiving">Diving</a></li>
				<li><a href="/cleaning/index.htm" id="headerContainerhttpwwwhsegovukcleaningindexhtmCleaning">Cleaning</a></li>
				<li><a href="/treework/index.htm" id="headerContainerhttpwwwhsegovuktreeworkindexhtmTreework">Treework</a></li>
				<li><a href="/entertainment/index.htm" id="headerContainerhttpwwwhsegovukentertainmentindexhtmEntertainment">Entertainment</a></li>
				<li><a href="/hairdressing/index.htm" id="headerContainerhttpwwwhsegovukhairdressingindexhtmHairdressing">Hairdressing</a></li>
			</ul> 
        	<p class="more"><a href="/guidance/industries.htm" id="headerContainerhttpwwwhsegovukguidanceindustrieshtmMoreindustries">More industries...</a></p>
		</div>                           
	</div><div class="navColRight"> 
		<div class="featureBox">
			<h2>Featured site</h2>
			<h3>Controlling risks</h3>
			<a href="/toolbox/index.htm" id="headerContainerhttpwwwhsegovuktoolboxindexhtm">
				<img class="navColFeaturedImage" src="http://www.hse.gov.uk/assets/v4-images/website/dropdown/dropFeatured.png" alt="Health and safety toolbox: How to control risks at work">
			</a>
			<p class="navColFeaturedArrow">Need help? Start with the health and safety toolbox: How to control risks at work.</p>
			<p class="more"><a href="/toolbox/index.htm" id="headerContainerhttpwwwhsegovuktoolboxindexhtmToolbox">Toolbox</a></p>
		</div>
	</div></div></div></li>
        <li class="aboutyou"><a href="aboutyou/index.htm" id="headerContainerhttpwwwhsegovukaboutyouindexhtmAboutyou">About you</a><span class="arrow"></span><div class="navDrop navDropAboutYou"><span class="navDropGap"></span><div class="navDropContent cf"><ul>
      <li><a href="/consult/index.htm" id="headerContainerhttpwwwhsegovukconsultindexhtmConsultations">Consultations</a></li>
      <li><a href="/aboutyou/webcom.htm" id="headerContainerhttpwwwhsegovukaboutyouwebcomhtmCommunities">Communities</a></li>
      <li><a href="/aboutyou/areyou.htm" id="headerContainerhttpwwwhsegovukaboutyouareyouhtmAreyou">Are you...</a></li>
      <li><a href="/aboutyou/howto.htm" id="headerContainerhttpwwwhsegovukaboutyouhowtohtmHowto">How to...</a></li>
      <li><a href="/aboutyou/website.htm" id="headerContainerhttpwwwhsegovukaboutyouwebsitehtmAboutHSEswebsite">About HSE's website</a></li>
    </ul></div></div></li>
        <li class="abouthse"><a href="aboutus/index.htm" id="headerContainerhttpwwwhsegovukaboutusindexhtmAboutHSE">About HSE</a><span class="arrow"></span><div class="navDrop navDropAboutHSE"><span class="navDropGap"></span><div class="navDropContent cf"><div class="navColLeft"> 
		<div class="menu1Col">
			<ul>
				<li><a href="/aboutus/insidehse.htm" id="headerContainerhttpwwwhsegovukaboutusinsidehsehtmInsideHSE">Inside HSE</a></li>
				<li><a href="/aboutus/people.htm" id="headerContainerhttpwwwhsegovukaboutuspeoplehtmHSEspeople">HSE's people</a></li>
				<li><a href="/careers/index.htm" id="headerContainerhttpwwwhsegovukcareersindexhtmHSEjobs">HSE jobs</a></li>
				<li><a href="/aboutus/hseswork.htm" id="headerContainerhttpwwwhsegovukaboutushsesworkhtmHSEswork">HSE's work</a></li>
				<li><a href="/aboutus/workingwith.htm" id="headerContainerhttpwwwhsegovukaboutusworkingwithhtmWorkingwithHSE">Working with<span class="hide"> HSE</span></a></li>
				<li><a href="/sellingtohse/index.htm" id="headerContainerhttpwwwhsegovuksellingtohseindexhtmSellingtoHSE">Selling to HSE</a></li>
				<li><a href="/aboutus/strategiesandplans/index.htm" id="headerContainerhttpwwwhsegovukaboutusstrategiesandplansindexhtmHSEsStrategiesandplans"><span class="hide">HSE's </span>Strategies and plans</a></li>
				<li><a href="/aboutus/timeline/index.htm" id="headerContainerhttpwwwhsegovukaboutustimelineindexhtmTimelineandahistoryofHSE">Timeline<span class="hide"> and a history of HSE</span></a></li>
			</ul>
		</div>
	</div><div class="navColRight"> 
		<div class="featureBox">
			<h2>HSE Jobs</h2>
			<h3>Calling all graduates</h3>
			<a href="/careers/subscribe.htm" id="headerContainerhttpwwwhsegovukcareerssubscribehtm">
				<img class="navColFeaturedImage" src="http://www.hse.gov.uk/assets/v4-images/website/dropdown/graduates1.jpg" alt="HSE jobs">
			</a>
			<p class="navColFeaturedArrow">HSE is shortly recruiting for Regulatory Scientists, sign up to our HSE jobs eBulletin for more information</p>
			<p class="more"><a href="/careers/subscribe.htm" id="headerContainerhttpwwwhsegovukcareerssubscribehtmSubscribetoHSEjobseBulletin">Subscribe to HSE jobs eBulletin</a></p>
		</div>
	</div></div></div></li>
          <li class="contact"><a href="contact/index.htm" id="headerContainerhttpwwwhsegovukcontactindexhtmContactHSE">Contact HSE</a><span class="arrow"></span><div class="navDrop navDropContact"><span class="navDropGap"></span><div class="navDropContent cf"><ul>
      <li><a href="/contact/contact.htm" id="headerContainerhttpwwwhsegovukcontactcontacthtmWaystocontactHSE">Ways to contact HSE</a></li>
      <li><a href="/riddor/index.htm" id="headerContainerhttpwwwhsegovukriddorindexhtmReportanincident">Report an incident</a></li>
	  <li><a href="/contact/concerns.htm" id="headerContainerhttpwwwhsegovukcontactconcernshtmReportaworkplaceconcern">Report a workplace concern</a></li>
      <li><a href="/contact/faqs/index.htm" id="headerContainerhttpwwwhsegovukcontactfaqsindexhtmFAQsYourtopquestions">FAQs<span class="hide"> - Your top questions</span></a></li>
      <li><a href="https://www.hse.gov.uk/forms/index.htm" id="headerContainerhttpswwwhsegovukformsindexhtmHSEForms">HSE Forms</a></li>
      <li><a href="/contact/maps/index.htm" id="headerContainerhttpwwwhsegovukcontactmapsindexhtmHSEoffices">HSE offices</a></li>
      <li><a href="/foi/index.htm" id="headerContainerhttpwwwhsegovukfoiindexhtmFreedomofinformation">Freedom of information</a></li>
      <li><a href="/contact/complaints.htm" id="headerContainerhttpwwwhsegovukcontactcomplaintshtmComplaints">Complaints</a></li>
      <li><a href="/contact/confidentiality.htm" id="headerContainerhttpwwwhsegovukcontactconfidentialityhtmConfidentiality">Confidentiality</a></li>
    </ul></div></div></li>
		</ul>
        <div id="accessibilityPanel">
            <div id="accessibilityTab">
                <p><a href="accessibility.htm" id="accessibilityTabhttpwwwhsegovukaccessibilityhtmAccessibility">Accessibility</a></p>
            </div>	    
            <div id="textTab" style="display: block;">
                <p>Text size:</p>
                <ul>
                    <li><a class="normalFont" href="#" title="Default text size" id="textTabhttpwwwhsegovukAswitchtonormalsize">A <span class="hide">- switch to normal size</span></a></li>
                    <li><a class="largeFont" href="#" title="Larger text size" id="textTabhttpwwwhsegovukAswitchtolargesize">A <span class="hide">- switch to large size</span></a></li>
                    <li class="last"><a class="largerFont" href="#" title="Largest text size" id="textTabhttpwwwhsegovukAswitchtolargersize">A <span class="hide">- switch to larger size</span></a></li>
                </ul>
            </div>	
        </div>
	</div>
</div>
<div id="navDivider"></div>        





<div id="pageContainer" class="cf">




	<div id="socialMediaTop" class="socialMedia">
        <h2 class="hideFromScreen">Social media</h2>	
        <div class="socialBar">
            <div class="socialDropdown">
                
                <ul style="display: block;">
                    <li class="socialRate"><span>Rate this page</span> <a href="#" class="rating" id="rated-1" title="One star"><span class="rating1">One star</span></a> <a href="#" class="rating" id="rated-2" title="Two stars"><span class="rating2">Two stars</span></a> <a href="#" class="rating" id="rated-3" title="Three stars"><span class="rating3">Three stars</span></a> <a href="#" class="rating" id="rated-4" title="Four stars"><span class="rating4">Four stars</span></a> <a href="#" class="rating" id="rated-5" title="Five stars"><span class="rating5">Five stars</span></a></li>
                    <li class="socialShare"><a id="shareTop" href="#share-this-page"><span>Share</span></a></li>
                    <li class="socialUpdate"><a id="updateTop" href="#free-updates"><span>Free updates</span></a></li>					
                    <li class="socialBookmark"><a id="BookmarkTop" href="#bookmark-this-page"><span>Bookmark</span></a></li>
                </ul>
            </div>
            <div class="socialLinks">	
            	<p>Follow HSE on Twitter:</p><a href="https://twitter.com/H_S_E" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false" data-dnt="true" id="socialMediaTophttpstwittercomHSE"><img src="http://www.hse.gov.uk/assets/v4-images/website/socialmedia/twit-follow.png" width="60" height="20" alt="Follow @H_S_E"></a>
            </div>             
        </div>
        <div id="socialDynamicTop" class="socialDynamic"><div class="socialShareContent"><div class="socialCloseButton"><a href="#" id="socialMediaTophttpwwwhsegovuk"><img src="http://www.hse.gov.uk/assets/v4-images/website/socialmedia/close.png" alt="Close this panel"></a></div>
	<h2 id="share-this-page" tabindex="0">Share this page</h2>
	<div class="box socialBox" style="margin-right: 140px !important;">
		<div class="socialIcon">
			<img src="http://www.hse.gov.uk/assets/v4-images/website/socialmedia/shareTwitter.png" alt="">
		</div>	
		<div class="alignLeft socialTwitter">
			<h3>Post this page to Twitter</h3>            
            <p><a href="https://twitter.com/share?url=http%3A%2F%2Fwww.hse.gov.uk%2F&amp;text=HSE%3A%20Information%20about%20health%20and%20safety%20at%20work" class="twitter-share-button" id="socialMediaTophttpstwittercomshareurlhttp3A2F2Fwwwhsegovuk2FtextHSE3A20Information20about20health20and20safety20at20work"><img src="/assets/v4-images/website/socialmedia/twit-tweet.png" alt="Tweet this page" width="58" height="20"></a></p>
        </div>
	</div>
	<div class="box socialBox" style="margin-right: 140px !important;">
		<div class="socialIcon">
			<img src="/assets/v4-images/website/socialmedia/shareEmail.png" alt="">
		</div>	
		<div class="alignLeft socialEmail">
			<h3>Email a friend</h3>
            <p><a class="emailForm" href="#" id="socialMediaTophttpwwwhsegovukSendthispage">Send this page</a></p> 
		</div>
	</div>	
	<div class="box socialBox boxLast">
		<div class="socialIcon socialIconPrint">
			<img src="/assets/v4-images/website/socialmedia/sharePrint.png" alt="">
		</div>	
		<div class="alignLeft socialPrint">
			<h3>Print</h3>
			<p><a href="#" id="socialMediaTophttpwwwhsegovukPrintthispage">Print this page</a></p>
		</div>
	</div>
</div>

<div class="socialUpdateContent"><div class="socialCloseButton"><a href="#" id="socialMediaTophttpwwwhsegovuk"><img src="/assets/v4-images/website/socialmedia/close.png" alt="Close this panel"></a></div>
	<h2 id="free-updates" tabindex="0">Free updates from HSE</h2>
		<div class="box socialBox">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/updateEbulletin.png" alt="">
			</div>		
			<div class="alignLeft socialEbulletin">
				<h3>Free email news and updates</h3>
				<form action="http://public.govdelivery.com/accounts/UKHSE/subscribers/qualify">
                    <fieldset>
                        <label for="email-free-updates" class="hide">Email address</label>
                        <input id="email-free-updates" name="email" type="text" value="Email address">
                        <input class="button" name="commit" type="submit" value="Sign up">
                    </fieldset>
                </form>
			</div>
		</div>	
		<div class="box socialBox">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/updateRss.png" alt="">
			</div>	
			<div class="alignLeft socialRss">
				<h3>RSS feeds</h3>
				<ul>
					<li><a href="http://news.hse.gov.uk/" id="socialMediaTophttpnewshsegovukViewlatestupdates">View latest updates</a></li>
					<li><a href="http://news.hse.gov.uk/categories/" id="socialMediaTophttpnewshsegovukcategoriesSubscribetoyourcategory">Subscribe to your category</a></li>
				</ul>
			</div>
		</div>
		<div class="box socialBox boxLast">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/updatePodcast.png" alt="">
			</div>	
			<div class="alignLeft socialPodcast">
				<h3>Podcast</h3>
				<p><a href="http://www.hse.gov.uk/podcasts/" id="socialMediaTophttpwwwhsegovukpodcastsSubscribetofreeaudioupdates">Subscribe to free audio updates</a></p>
			</div>
		</div>			
</div>

<div class="socialBookmarkContent"><div class="socialCloseButton"><a href="#" id="socialMediaTophttpwwwhsegovuk"><img src="/assets/v4-images/website/socialmedia/close.png" alt="Close this panel"></a></div>
	<h2 id="bookmark-this-page" tabindex="0">Bookmark this page</h2>
			<div class="box socialBox">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/bookmarkBrowser.png" alt="">
			</div>	
			<div class="alignLeft socialBrowser">
				<h3>To your browser</h3>
				<p><a href="#" id="socialMediaTophttpwwwhsegovukAddtoyourfavourites">Add to your favourites</a></p>
			</div>
		</div>
		<div class="box socialBox">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/bookmarkDelicious.png" alt="">
			</div>	
			<div class="alignLeft socialDelicious">
				<h3>To delicious</h3>
				<p><img src="/assets/v4-images/website/socialmedia/delicious.small.gif" height="10" width="10" alt="Delicious">
<a href="http://www.delicious.com/save" onclick="window.open('http://www.delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title), 'delicious','toolbar=no,width=550,height=550'); return false;" id="socialMediaTophttpwwwdeliciouscomsaveBookmarkthisonDelicious">Bookmark this on Delicious</a></p>
			</div>
		</div>			
		<div class="box socialBox">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/bookmarkStumble.png" alt="">
			</div>	
			<div class="alignLeft socialStumble">
				<h3>To StumbleUpon</h3>
			<p><a href="http://stumbleupon.com/submit?url=http://www.hse.gov.uk/&amp;title=HSE: Information about health and safety at work" title="Share on Stumbleupon" id="socialMediaTophttpstumbleuponcomsubmiturlhttpwwwhsegovuktitleHSE20Information20about20health20and20safety20at20workShareonStumbleupon">Share on Stumbleupon</a></p></div>
		</div>
		<div class="box socialBox boxLast">
			<div class="socialIcon">
				<img src="/assets/v4-images/website/socialmedia/bookmarkReddit.png" alt="">
			</div>	
			<div class="alignLeft socialReddit">
				<h3>To reddit</h3>
				<p><a href="http://www.reddit.com/submit" onclick="window.location = 'http://www.reddit.com/submit?url=' + encodeURIComponent(window.location); return false" id="socialMediaTophttpwwwredditcomsubmit"><img src="/assets/v4-images/website/socialmedia/spreddit7.gif" alt="submit to reddit" border="0"></a></p>
			</div>
		</div>	
</div>

<div class="socialDialogRate socialDialogRateNegative hideFromScreen" title="Would you like to give feedback about this page?">
	<div class="icon"></div>
	<p>Thanks for taking the time to share your views with HSE. We will use feedback to improve our pages wherever possible.</p>
    <p>Please select from one of the following:</p>
    
    <ul>
    	<li><a id="negativeDifficultToFind" href="#">The page was difficult to find.</a></li>
        <li><a id="negativeDidntDisplayProperly" href="#">The page did not display properly.</a></li>
        <li><a id="negativeDidntAnswerQuestion" href="#">The page did not answer my question(s).</a></li>
        <li><a id="negativePageInaccurate" href="#">The page is inaccurate.</a></li>
        <li><a id="negativePageNotEasyToUse" href="#">The page is not easy to use.</a></li>
        <li><a id="negativePageBrokenLinks" href="#">The page contains 1 or more broken links.</a></li>        
	</ul>
 
</div>

<div class="socialDialogRate socialDialogRateNeutral hideFromScreen" title="Would you like to give feedback about this page?">
	<div class="icon"></div>
    <p>Thanks for taking the time to share your views with HSE. We will use feedback to improve our pages wherever possible.</p>
    <p>Please select one area that needs improvement:</p>
    
    <ul>
    	<li><a id="neutralFindingPage" href="#">Finding the page.</a></li>
        <li><a id="neutralPageLayout" href="#">Page layout.</a></li>
        <li><a id="neutralUsefulContent" href="#">Useful content.</a></li>
        <li><a id="neutralAccuracy" href="#">Accuracy.</a></li>
        <li><a id="neutralEaseOfUse" href="#">Ease of use.</a></li>
        <li><a id="neutralPageBrokenLinks" href="#">The page contains 1 or more broken links.</a></li>        
	</ul>
</div>

<div class="socialDialogRate socialDialogRatePositive hideFromScreen" title="Would you like to give feedback about this page?">
	<div class="icon"></div>
    <p>Thanks for taking the time to share your views with HSE. We will use feedback to improve our pages wherever possible.</p>
    <p>Please select from one of the following:</p>
    
    <ul>
    	<li><a id="positiveEasyToFind" href="#">The page was easy to find.</a></li>
        <li><a id="positivePageDisplayedProperly" href="#">The page displayed properly.</a></li>
        <li><a id="positivePageAnsweredQuestion" href="#">The page answered my question(s).</a></li>
        <li><a id="positivePageAccurate" href="#">The page is accurate.</a></li>
        <li><a id="positivePageEasyToUse" href="#">The page is easy to use.</a></li>
	</ul>    
</div>

<div class="socialDialogEmail hideFromScreen" title="Email a friend">
	<div class="icon"></div>
	<p>Use this free service to easily send a link to this page by email.</p>
	<form action="http://blogs.hse.gov.uk/email/mail.php" method="post">
	<fieldset>
		<label for="name-email-a-friend">Your name</label>
		<input type="text" name="name" id="name-email-a-friend" class="text ui-widget-content ui-corner-all">
        <label for="email-a-friend">Your email</label>
		<input type="text" name="email" id="email-a-friend" value="" class="text ui-widget-content ui-corner-all">
        <label for="recipient-email-a-friend">Friends name</label>
		<input type="text" name="recipient" id="recipient-email-a-friend" class="text ui-widget-content ui-corner-all">
        <label for="sendto-email-a-friend">Friends email</label>
		<input type="text" name="sendto" id="sendto-email-a-friend" value="" class="text ui-widget-content ui-corner-all">
        <label for="validate" class="hideFromScreen">Leave this blank</label>
		<input type="text" name="validate" id="validate" value="" class="hideFromScreen text ui-widget-content ui-corner-all">
		<label for="message-email-a-friend">Additional comments</label>
		<textarea name="message" id="message-email-a-friend" class="text ui-widget-content ui-corner-all"></textarea>
        <input class="hiddenUrl" type="hidden" name="url" value="">
	</fieldset>
	</form>
</div></div>
	</div>

	
   
	
    <div id="contentContainer" class="column1" role="main">

<!-- InstanceBeginEditable name="content" -->
<div class="box box3col boxNoBackground boxSlider">
    <div class="sliderPreLoad" style="display: none;"><a href="careers/jobprofiles.htm" id="contentContainerhttpwwwhsegovukcareersjobprofileshtm"><img src="assets/v4-homepage/slider-ie6.jpg" alt="Make an impact - influence safety management with a career in HSE"></a><sup class="hideFromScreen">[1]</sup>
    </div>
    
    <div class="homepageSlider rsDefault rsHor rsWebkit3d" style="display: block;"><div class="rsOverflow" style="cursor: -webkit-grab; width: 698px; height: 253px;"><div class="rsContainer" style="-webkit-transition-duration: 400ms; transition-duration: 400ms; -webkit-transform: translate3d(-7678px, 0px, 0px); -webkit-transition-timing-function: cubic-bezier(0.445, 0.05, 0.55, 0.95); transition-timing-function: cubic-bezier(0.445, 0.05, 0.55, 0.95);"><div style="left: 6980px;" class="rsSlide "><div class="rsContent" style="-webkit-transition: opacity 400ms ease-in-out; transition: opacity 400ms ease-in-out; visibility: visible; opacity: 1;">
			<a href="careers/jobprofiles.htm" id="contentContainerhttpwwwhsegovukcareersjobprofileshtmMakeanimpactInfluencesafetymanagementwithacareerinHSE">
				<img class="rsImage" src="assets/v4-homepage/slider/b3-campaign-people.jpg" alt="">
				<h2 class="rsABlock slideOffshoreCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="100" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">Make an impact</h2>
				<p class="rsABlock slideOffshoreCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="200" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">Influence safety management with a<span class="link"> career in HSE</span></p>
		  </a><sup class="hideFromScreen">[2]</sup>
				
		</div></div><div style="left: 7678px;" class="rsSlide "><div class="rsContent">
			<a href="http://www.beware-asbestos.info/02" id="contentContainerhttpwwwbewareasbestosinfo0220tradespeopleonaveragedieeveryweekfromasbestosrelateddiseasesProtectyourselfandyourworkmateswiththefreeBewareAsbestoswebapp">
				<img class="rsImage" src="assets/v4-homepage/slider/asbestos-app.jpg" alt="">
				<h2 class="rsABlock slideSubscribeCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="150" data-delay="100" style="display: block; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 400ms; transition-duration: 400ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">20 tradespeople, on average, die every week<br>
                from asbestos-related diseases</h2>
				<p class="rsABlock slideSubscribeCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="200" style="display: block; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 400ms; transition-duration: 400ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">Protect yourself and your workmates<br>
 with the free <span class="link">Beware Asbestos web app</span></p>
				 <img class="fileIcon" src="/assets/v4-images/icons/external.gif" alt="link to external website"></a><sup class="hideFromScreen">[3]</sup>
				
			</div></div><div style="left: 8376px;" class="rsSlide "><div class="rsContent">
        	<a href="coshh/essentials/index.htm" id="contentContainerhttpwwwhsegovukcoshhessentialsindexhtmCOSHHessentialsetoolNewandimprovedetoolwithHazardstatements">
                <img class="rsImage" src="assets/v4-homepage/slider/coshh-lung.jpg" alt="">
                <h2 class="rsABlock slideRiddorCaption" data-delay="100" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">COSHH essentials e-tool</h2>
                <p class="rsABlock slideRiddorCaption" data-delay="200" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">New and improved <span class="link">e-tool</span> with Hazard statements</p>
                </a><sup class="hideFromScreen">[4]</sup>
            
        </div></div><div style="left: 6282px;" class="rsSlide "><div class="rsContent">
			<a href="myth/index.htm" id="contentContainerhttpwwwhsegovukmythindexhtmHealthandsafetymythsMythBustersChallengePanelresponsestocommonhealthandsafetymyths">
				<img class="rsImage" src="assets/v4-homepage/slider/mythbusting.jpg" alt="">         
				<h2 class="rsABlock slideSubscribeCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="100" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">Health and safety myths</h2>
				<p class="rsABlock slideSubscribeCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="0" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">Myth Busters Challenge Panel - responses to <br><span class="link">common health and safety myths</span> </p>
				</a><sup class="hideFromScreen">[5]</sup>  
			         
        </div></div><div style="left: 9074px;" class="rsSlide "><div class="rsContent">
			<a href="pubns/books/l153.htm" id="contentContainerhttpwwwhsegovukpubnsbooksl153htmCDM2015InformationonthenewConstructionDesignandManagementRegulations2015thatcameintoforceon6April2015">
				<img class="rsImage" src="assets/v4-homepage/slider/cdm-2015.jpg" alt="">         
				<h2 class="rsABlock slideExampleRiskCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="100" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">CDM 2015</h2>
				<p class="rsABlock slideExampleRiskCaption" data-fade-effect="true" data-move-effect="top" data-move-offset="200" data-delay="0" style="display: none; -webkit-transform: translate3d(0px, 0px, 0px); opacity: 1; -webkit-transition-property: -webkit-transform, opacity; transition-property: -webkit-transform, opacity; -webkit-transition-duration: 0ms; transition-duration: 0ms; -webkit-transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1); transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);">Information on the new <span class="link">Construction (Design and Management) <br>Regulations 2015</span> that came into force on 6 April 2015 </p>
				</a><sup class="hideFromScreen">[6]</sup>  
			         
        </div></div></div><div class="rsArrow rsArrowLeft rsHidden" style="display: block;"><div class="rsArrowIcn"></div></div><div class="rsArrow rsArrowRight rsHidden" style="display: block;"><div class="rsArrowIcn"></div></div></div><div class="rsNav rsTabs"><div class="rsNavItem rsTab"><div class="rsTmb">HSE jobs</div></div><div class="rsNavItem rsTab rsNavSelected"><div class="rsTmb">Beware Asbestos</div></div><div class="rsNavItem rsTab"><div class="rsTmb">COSHH e-tool</div></div><div class="rsNavItem rsTab"><div class="rsTmb">CDM 2015</div></div><div class="rsNavItem rsTab"><div class="rsTmb">Myths</div></div></div></div>	
<a id="sliderControl" class="sliderControl sliderOn" href="#"><img src="/assets/v4-homepage/slider/pause-icon.png" alt="Pause slider"></a></div>

<div class="box box1col boxLast hasMore">
	<h2>Are you...</h2>
    <ul id="chevron-short" class="chevron-homepage">
        <li><span><a href="abc/index.htm" id="contentContainerhttpwwwhsegovukabcindexhtmNewtohealthandsafety">New to health <br>and safety?</a><sup class="hideFromScreen">[7]</sup></span></li>
        <li><span><a href="simple-health-safety/index.htm" id="contentContainerhttpwwwhsegovuksimplehealthsafetyindexhtmAlowriskbusiness">A low-risk business?</a><sup class="hideFromScreen">[8]</sup></span></li>
        <li><span><a href="workers/index.htm" id="contentContainerhttpwwwhsegovukworkersindexhtmAnemployee">An employee?</a><sup class="hideFromScreen">[9]</sup></span></li>
    </ul>    
    <p class="more"><a href="simple-health-safety/index.htm" id="contentContainerhttpwwwhsegovuksimplehealthsafetyindexhtmThingsyouneedtodo">Things you need to do</a><sup class="hideFromScreen">[10]</sup></p>
</div>

<div class="box box2col homepageNews equalHeightsRow2 hasMore" style="height: 260px;">
	<h2>Latest news</h2>
    <ul>
    	<li>
        	<a href="statistics/fatals.htm" id="contentContainerhttpwwwhsegovukstatisticsfatalshtmFatalinjurystatisticssummaryfor201415ProvisionalstatisticsonfatalinjuriesintheworkplaceinGreatBritain">
        	<img src="assets/v4-homepage/news/numbers1.jpg" alt="">
    		<span class="header">Fatal injury statistics - summary for 2014/15</span>
    		<span class="subText">Provisional statistics on fatal injuries in the workplace in Great Britain</span>
            </a><sup class="hideFromScreen">[11]</sup>
           
        </li>
    	<li>
        	<a href="aboutus/reports/ara-2014-15.htm" id="contentContainerhttpwwwhsegovukaboutusreportsara201415htmPublicationofHSEAnnualreportandaccounts201415SummarisesHSEsperformanceduring201415">
        	<img src="assets/v4-homepage/news/freeguidance.jpg" alt="">
    		<span class="header">Publication of HSE Annual report and accounts 2014/15</span>
    		<span class="subText">Summarises HSE's performance during 2014/15</span>
            </a><sup class="hideFromScreen">[12]</sup>
        </li>
    
		<!--<li class="last">
			<a href="construction/campaigns/safersites/index.htm">
				<img src="assets/v4-homepage/news/safersites-infographic8.jpg" alt="" />
				<span class="header">Safersites</span>
				<span class="subText">Find out about HSE's construction health focussed inspection initiative...</span>
			</a>
		</li>-->
		
<!--		<li class="last">
			<a href="consult/condocs/cd270.htm">
				<img src="assets/v4-homepage/news/pressure-gauges.jpg" alt="" />
				<span class="header">public consultation - Pressure systems</span>
				<span class="subText">Consultation on draft <abbr title="Approved Code of Practice">ACOP</abbr> - Safety of pressure systems (L122)</span>
			</a>
		</li>-->
        

        
        
    	
    </ul>
    <p class="more"><a href="news/index.htm" id="contentContainerhttpwwwhsegovuknewsindexhtmMorenews">More news</a><sup class="hideFromScreen">[13]</sup></p>    
</div>

<div class="box box1col equalHeightsRow2 hasMore" style="height: 260px;">
	<h2>I am interested in...</h2>
    <ul class="linkList">
    	<li><a href="coshh/index.htm" id="contentContainerhttpwwwhsegovukcoshhindexhtmCOSHH"><abbr title="Control of Substances Hazardous to Health">COSHH</abbr></a><sup class="hideFromScreen">[14]</sup></li>
    	<li><a href="riddor/index.htm" id="contentContainerhttpwwwhsegovukriddorindexhtmRIDDOR"><abbr title="Reporting of Injuries, Diseases and Dangerous Occurrences Regulations">RIDDOR</abbr></a><sup class="hideFromScreen">[15]</sup></li>
    	<li><a href="risk/index.htm" id="contentContainerhttpwwwhsegovukriskindexhtmRiskassessment">Risk assessment</a><sup class="hideFromScreen">[16]</sup></li>
    	<li><a href="contact/index.htm" id="contentContainerhttpwwwhsegovukcontactindexhtmRaisingaconcern">Raising a concern</a><sup class="hideFromScreen">[17]</sup></li>
    	<li><a href="asbestos/index.htm" id="contentContainerhttpwwwhsegovukasbestosindexhtmAsbestos">Asbestos</a><sup class="hideFromScreen">[18]</sup></li>
    	<li><a href="stress/index.htm" id="contentContainerhttpwwwhsegovukstressindexhtmStress">Stress</a><sup class="hideFromScreen">[19]</sup></li>   
    	<li><a href="comah/index.htm" id="contentContainerhttpwwwhsegovukcomahindexhtmCOMAH"><abbr title="Control of major accident hazards">COMAH</abbr></a><sup class="hideFromScreen">[20]</sup></li>
    	<li><a href="temperature/index.htm" id="contentContainerhttpwwwhsegovuktemperatureindexhtmTemperature">Temperature</a><sup class="hideFromScreen">[21]</sup></li>
    	<li><a href="electricity/index.htm" id="contentContainerhttpwwwhsegovukelectricityindexhtmElectricity">Electricity</a><sup class="hideFromScreen">[22]</sup></li>                        
    </ul>
    <p class="more"><a href="guidance/topics.htm" id="contentContainerhttpwwwhsegovukguidancetopicshtmMoretopics">More topics</a><sup class="hideFromScreen">[23]</sup></p>
</div>

<div class="box box1col boxLast equalHeightsRow2 hasMore" style="height: 260px;">
	<h2>I work in...</h2>  
    <ul class="linkList">
    	<li><a href="construction/index.htm" id="contentContainerhttpwwwhsegovukconstructionindexhtmConstruction">Construction</a><sup class="hideFromScreen">[24]</sup></li>
    	<li><a href="offshore/index.htm" id="contentContainerhttpwwwhsegovukoffshoreindexhtmOffshore">Offshore</a><sup class="hideFromScreen">[25]</sup></li>
    	<li><a href="http://www.onr.org.uk/index.htm" id="contentContainerhttpwwwonrorgukindexhtmNuclear">Nuclear <img class="fileIcon" src="/assets/v4-images/icons/external.gif" alt="link to external website"></a><sup class="hideFromScreen">[26]</sup></li>
    	<li><a href="healthservices/index.htm" id="contentContainerhttpwwwhsegovukhealthservicesindexhtmHealthsocialcareservices">Health &amp; social care services</a><sup class="hideFromScreen">[27]</sup></li>
    	<li><a href="services/education/index.htm" id="contentContainerhttpwwwhsegovukserviceseducationindexhtmEducation">Education</a><sup class="hideFromScreen">[28]</sup></li>     
    	<li><a href="agriculture/index.htm" id="contentContainerhttpwwwhsegovukagricultureindexhtmAgriculture">Agriculture</a><sup class="hideFromScreen">[29]</sup></li>
    	<li><a href="quarries/index.htm" id="contentContainerhttpwwwhsegovukquarriesindexhtmQuarries">Quarries</a><sup class="hideFromScreen">[30]</sup></li>
    	<li><a href="catering/index.htm" id="contentContainerhttpwwwhsegovukcateringindexhtmCatering">Catering</a><sup class="hideFromScreen">[31]</sup></li>
    	<li><a href="engineering/index.htm" id="contentContainerhttpwwwhsegovukengineeringindexhtmEngineering">Engineering</a><sup class="hideFromScreen">[32]</sup></li>              
    </ul>    
    <p class="more"><a href="guidance/industries.htm" id="contentContainerhttpwwwhsegovukguidanceindustrieshtmMoreindustries">More industries</a><sup class="hideFromScreen">[33]</sup></p>        
</div>

<div class="box homepageRecommended">
	<h2>Recommended</h2>
    <div id="homepageCarousel" class="touchcarousel black-and-white" style="overflow: visible;">     
        <div class="touchcarousel-wrapper grab-cursor"><ul class="touchcarousel-container" style="-webkit-transform-origin: 0px 0px 0px; -webkit-transform: translateZ(0px); width: 1190px;">
            
            <li class="touchcarousel-item">
                <a href="coshh/essentials/index.htm" id="contentContainerhttpwwwhsegovukcoshhessentialsindexhtmCOSHHessentialsetool">
					<img src="assets/v4-homepage/carousel/coshh-lung.jpg" width="100" height="140" alt="">
					COSHH essentials e-tool</a><sup class="hideFromScreen">[34]</sup>
       	   	</li>
            
            <li class="touchcarousel-item">
                <a href="pubns/books/newsletter.htm" id="contentContainerhttpwwwhsegovukpubnsbooksnewsletterhtmHealthandSafetyNewsletter">
					<img src="assets/v4-homepage/carousel/newsletter.jpg" width="100" height="140" alt="">
					Health and Safety Newsletter</a><sup class="hideFromScreen">[35]</sup>
       	   	</li>
        
        	<li class="touchcarousel-item">
                <a href="pubns/books/l153.htm" id="contentContainerhttpwwwhsegovukpubnsbooksl153htmManaginghealthandsafetyinconstruction">
					<img src="assets/v4-homepage/carousel/l153.jpg" width="100" height="140" alt="">
					Managing health and safety in construction</a><sup class="hideFromScreen">[36]</sup>
   	      	</li>
       
   	  <li class="touchcarousel-item">
                <a href="pubns/books/l29.htm" id="contentContainerhttpwwwhsegovukpubnsbooksl29htmGMORegulations2014">
					<img src="assets/v4-homepage/carousel/l29.jpg" width="100" height="140" alt="">
					<abbr title="Genetically Modified Organisms">GMO</abbr>  Regulations 2014</a><sup class="hideFromScreen">[37]</sup>
   		  </li>
            
       		<li class="touchcarousel-item">
                <a href="pubns/books/l150.htm" id="contentContainerhttpwwwhsegovukpubnsbooksl150htmExplosivesRegulations2014">
					<img src="assets/v4-homepage/carousel/l150.jpg" width="100" height="140" alt="">
					Explosives Regulations 2014</a><sup class="hideFromScreen">[38]</sup>
          	</li>
			
            <li class="touchcarousel-item">
                <a href="pubns/books/hsg65.htm" id="contentContainerhttpwwwhsegovukpubnsbookshsg65htmManagingforhealthandsafety">
					<img src="assets/v4-homepage/carousel/hsg65.jpg" width="100" height="140" alt="">
					Managing for health and safety</a><sup class="hideFromScreen">[39]</sup>
      		</li>
            
            <li class="touchcarousel-item">
                <a href="pubns/books/hsg268.htm" id="contentContainerhttpwwwhsegovukpubnsbookshsg268htmThehealthandsafetytoolbox">
					<img src="assets/v4-homepage/carousel/hsg268.jpg" width="100" height="140" alt="">
					The health and safety toolbox</a><sup class="hideFromScreen">[40]</sup>
            </li>   
      
      		<li class="touchcarousel-item">
                <a href="pubns/indg163.htm" id="contentContainerhttpwwwhsegovukpubnsindg163htmRiskassessmentAbriefguide">
					<img src="assets/v4-homepage/carousel/indg163.jpg" width="100" height="140" alt="">
					Risk assessment - A brief guide</a><sup class="hideFromScreen">[41]</sup>
            </li>              
            
            <li class="touchcarousel-item">
                <a href="pubns/indg453.htm" id="contentContainerhttpwwwhsegovukpubnsindg453htmRIDDORReportanincident">
					<img src="assets/v4-homepage/carousel/indg453.jpg" width="100" height="140" alt="">
					RIDDOR - Report an incident</a><sup class="hideFromScreen">[42]</sup>
            </li>   
      		
            <li class="touchcarousel-item last">
                <a href="pubns/indg327.htm" id="contentContainerhttpwwwhsegovukpubnsindg327htmWorkingsafelywithacetylene">
					<img src="assets/v4-homepage/carousel/indg327.jpg" width="100" height="140" alt="">
					Working safely with acetylene</a><sup class="hideFromScreen">[43]</sup>
            </li>
          
        </ul></div><a href="#" id="arrow-holder-left" class="arrow-holder left disabled" title="Previous" aria-hidden="true"><span class="arrow-icon left"></span></a> <a href="#" id="arrow-holder-right" class="arrow-holder right" title="Next" aria-hidden="true"><span class="arrow-icon right"></span></a>
    <div class="scrollbar-holder"><div class="scrollbar dark" style="width: 702px;"></div></div></div>      
</div>



<!-- InstanceEndEditable -->

	<h2 class="hideFromScreen footnotes">Link URLs in this page</h2><ol class="hideFromScreen"><li>Make an impact - influence safety management with a career in HSE<br>http://www.hse.gov.uk/careers/jobprofiles.htm</li><li>Make an impact
				Influence safety management with a career in HSE<br>http://www.hse.gov.uk/careers/jobprofiles.htm</li><li>20 tradespeople, on average, die every week
                from asbestos-related diseases
				Protect yourself and your workmates
 with the free Beware Asbestos web app<br>http://www.beware-asbestos.info/02</li><li>COSHH essentials e-tool
                New and improved e-tool with Hazard statements<br>http://www.hse.gov.uk/coshh/essentials/index.htm</li><li>Health and safety myths
				Myth Busters Challenge Panel - responses to common health and safety myths<br>http://www.hse.gov.uk/myth/index.htm</li><li>CDM 2015
				Information on the new Construction (Design and Management) Regulations 2015 that came into force on 6 April 2015<br>http://www.hse.gov.uk/pubns/books/l153.htm</li><li>New to health and safety?<br>http://www.hse.gov.uk/abc/index.htm</li><li>A low-risk business?<br>http://www.hse.gov.uk/simple-health-safety/index.htm</li><li>An employee?<br>http://www.hse.gov.uk/workers/index.htm</li><li>Things you need to do<br>http://www.hse.gov.uk/simple-health-safety/index.htm</li><li>Fatal injury statistics - summary for 2014/15
    		Provisional statistics on fatal injuries in the workplace in Great Britain<br>http://www.hse.gov.uk/statistics/fatals.htm</li><li>Publication of HSE Annual report and accounts 2014/15
    		Summarises HSE's performance during 2014/15<br>http://www.hse.gov.uk/aboutus/reports/ara-2014-15.htm</li><li>More news<br>http://www.hse.gov.uk/news/index.htm</li><li>COSHH<br>http://www.hse.gov.uk/coshh/index.htm</li><li>RIDDOR<br>http://www.hse.gov.uk/riddor/index.htm</li><li>Risk assessment<br>http://www.hse.gov.uk/risk/index.htm</li><li>Raising a concern<br>http://www.hse.gov.uk/contact/index.htm</li><li>Asbestos<br>http://www.hse.gov.uk/asbestos/index.htm</li><li>Stress<br>http://www.hse.gov.uk/stress/index.htm</li><li>COMAH<br>http://www.hse.gov.uk/comah/index.htm</li><li>Temperature<br>http://www.hse.gov.uk/temperature/index.htm</li><li>Electricity<br>http://www.hse.gov.uk/electricity/index.htm</li><li>More topics<br>http://www.hse.gov.uk/guidance/topics.htm</li><li>Construction<br>http://www.hse.gov.uk/construction/index.htm</li><li>Offshore<br>http://www.hse.gov.uk/offshore/index.htm</li><li>Nuclear<br>http://www.onr.org.uk/index.htm</li><li>Health &amp; social care services<br>http://www.hse.gov.uk/healthservices/index.htm</li><li>Education<br>http://www.hse.gov.uk/services/education/index.htm</li><li>Agriculture<br>http://www.hse.gov.uk/agriculture/index.htm</li><li>Quarries<br>http://www.hse.gov.uk/quarries/index.htm</li><li>Catering<br>http://www.hse.gov.uk/catering/index.htm</li><li>Engineering<br>http://www.hse.gov.uk/engineering/index.htm</li><li>More industries<br>http://www.hse.gov.uk/guidance/industries.htm</li><li>COSHH essentials e-tool<br>http://www.hse.gov.uk/coshh/essentials/index.htm</li><li>Health and Safety Newsletter<br>http://www.hse.gov.uk/pubns/books/newsletter.htm</li><li>Managing health and safety in construction<br>http://www.hse.gov.uk/pubns/books/l153.htm</li><li>GMO  Regulations 2014<br>http://www.hse.gov.uk/pubns/books/l29.htm</li><li>Explosives Regulations 2014<br>http://www.hse.gov.uk/pubns/books/l150.htm</li><li>Managing for health and safety<br>http://www.hse.gov.uk/pubns/books/hsg65.htm</li><li>The health and safety toolbox<br>http://www.hse.gov.uk/pubns/books/hsg268.htm</li><li>Risk assessment - A brief guide<br>http://www.hse.gov.uk/pubns/indg163.htm</li><li>RIDDOR - Report an incident<br>http://www.hse.gov.uk/pubns/indg453.htm</li><li>Working safely with acetylene<br>http://www.hse.gov.uk/pubns/indg327.htm</li></ol><h2 class="hideFromScreen footnotes">Glossary of abbreviations/acronyms on this page</h2><dl class="hideFromScreen"></dl></div>




</div>

<div id="footerContainer" role="contentinfo">
   <div id="footer">
        <h2 class="hide">Footer links</h2>
        <ul>
              <li class="first"><a href="search/search-results.htm" id="footerContainerhttpwwwhsegovuksearchsearchresultshtmSearch">Search</a></li>
              <li><a href="a-z/index.htm" id="footerContainerhttpwwwhsegovukazindexhtmAZ">A-Z</a></li>
              <li><a href="acronym/index.htm" id="footerContainerhttpwwwhsegovukacronymindexhtmAcronyms">Acronyms</a></li>
              <li><a href="about/site_map/index.htm" id="footerContainerhttpwwwhsegovukaboutsitemapindexhtmSitemap">Site map</a></li>
              <li><a href="copyright.htm" id="footerContainerhttpwwwhsegovukcopyrighthtmCopyright">Copyright</a></li>
              <li><a href="disclaimer.htm" id="footerContainerhttpwwwhsegovukdisclaimerhtmDisclaimer">Disclaimer</a></li>
              <li><a href="privacy.htm" id="footerContainerhttpwwwhsegovukprivacyhtmPrivacy">Privacy</a></li>
              <li><a href="privacy-cookies.htm" id="footerContainerhttpwwwhsegovukprivacycookieshtmCookies">Cookies</a></li>
              <li><a href="accessibility.htm" id="footerContainerhttpwwwhsegovukaccessibilityhtmAccessibility">Accessibility</a></li>
        </ul>
   </div>
    <div id="baseStrip">
        <div id="baseStripInner">
            <p>HSE aims to reduce work-related death, injury and ill health.</p>
            <div id="languagesDropdown-container" class="ddmswitchContainerWithImages" style="height: 20px;"><div id="languagesDropdown-trigger" class="trigger" title="View the options" style="cursor: pointer;"></div><div class="ddmlabel">Information in:</div><ul class="ddmswitch separateLabel fauxDropdown" title="Information in:" id="languagesDropdown" style="height: 20px;">
                
                <li style="height: 20px;"><a href="languages/arabic/index.htm" id="footerContainerhttpwwwhsegovuklanguagesarabicindexhtmArabic"><span id="lArabic" class="nonLatin"></span> / Arabic</a></li>
                <li style="height: 20px;"><a href="languages/bengali/index.htm" id="footerContainerhttpwwwhsegovuklanguagesbengaliindexhtmBengali"><span id="lBengali" class="nonLatin"></span> / Bengali</a></li>
                <li style="height: 20px;"><a href="languages/bulgarian/index.htm" id="footerContainerhttpwwwhsegovuklanguagesbulgarianindexhtmBalgarskiBulgarian"><span lang="bg">Balgarski</span> / Bulgarian</a></li>
                <li style="height: 20px;"><a href="languages/chinese/index.htm" id="footerContainerhttpwwwhsegovuklanguageschineseindexhtmChinese"><span id="lChinese" class="nonLatin"></span> / Chinese</a></li>
                <li style="height: 20px;"><a href="languages/czech/index.htm" id="footerContainerhttpwwwhsegovuklanguagesczechindexhtmetinaCzech"><span lang="cs">Čeština</span> / Czech</a></li>
                <li style="height: 20px;"><a href="languages/farsi/index.htm" id="footerContainerhttpwwwhsegovuklanguagesfarsiindexhtmFarsi"><span id="lFarsi" class="nonLatin"></span> / Farsi</a></li>
                <li style="height: 20px;"><a href="languages/french/index.htm" id="footerContainerhttpwwwhsegovuklanguagesfrenchindexhtmFranaisFrench"><span lang="fr">Français</span> / French</a></li>
                <li style="height: 20px;"><a href="languages/greek/index.htm" id="footerContainerhttpwwwhsegovuklanguagesgreekindexhtmGreek"><span lang="el">Ελληνικά</span> / Greek</a></li>
                <li style="height: 20px;"><a href="languages/gujarati/index.htm" id="footerContainerhttpwwwhsegovuklanguagesgujaratiindexhtmGujarati"><span id="lGujarati" class="nonLatin"></span> / Gujarati</a></li>
                <li style="height: 20px;"><a href="languages/hindi/index.htm" id="footerContainerhttpwwwhsegovuklanguageshindiindexhtmHindi"><span id="lHindi" class="nonLatin"></span> / Hindi</a></li>
                <li style="height: 20px;"><a href="languages/kurdish/index.htm" id="footerContainerhttpwwwhsegovuklanguageskurdishindexhtmKurdish"><span id="lKurdish" class="nonLatin"></span> / Kurdish</a></li>
                <li style="height: 20px;"><a href="languages/latvian/index.htm" id="footerContainerhttpwwwhsegovuklanguageslatvianindexhtmLatvieuLatvian"><span lang="lv">Latviešu</span> / Latvian</a></li>
                <li style="height: 20px;"><a href="languages/lithuanian/index.htm" id="footerContainerhttpwwwhsegovuklanguageslithuanianindexhtmLietuviskaiLithuanian"><span lang="lt">Lietuviskai</span> / Lithuanian</a></li>
                <li style="height: 20px;"><a href="languages/pashto/index.htm" id="footerContainerhttpwwwhsegovuklanguagespashtoindexhtmPashto"><span id="lPashto" class="nonLatin"></span> / Pashto</a></li>
                <li style="height: 20px;"><a href="languages/polish/index.htm" id="footerContainerhttpwwwhsegovuklanguagespolishindexhtmPolskiPolish"><span lang="pl">Polski</span> / Polish</a></li>
                <li style="height: 20px;"><a href="languages/portuguese/index.htm" id="footerContainerhttpwwwhsegovuklanguagesportugueseindexhtmPortugusPortuguese"><span lang="pt">Português</span> / Portuguese</a></li>
                <li style="height: 20px;"><a href="languages/punjabi/index.htm" id="footerContainerhttpwwwhsegovuklanguagespunjabiindexhtmPunjabi"><span id="lPunjabi" class="nonLatin"></span> / Punjabi</a></li>
                <li style="height: 20px;"><a href="languages/romanian/index.htm" id="footerContainerhttpwwwhsegovuklanguagesromanianindexhtmRomnRomanian"><span lang="ro">Românâ</span> / Romanian</a></li>
                <li style="height: 20px;"><a href="languages/russian/index.htm" id="footerContainerhttpwwwhsegovuklanguagesrussianindexhtmRussian"><span lang="ru">Русский</span> / Russian</a></li>
                <li style="height: 20px;"><a href="languages/slovak/index.htm" id="footerContainerhttpwwwhsegovuklanguagesslovakindexhtmSlovenskySlovak"><span lang="sk">Slovensky</span> / Slovak</a></li>
                <li style="height: 20px;"><a href="languages/turkish/index.htm" id="footerContainerhttpwwwhsegovuklanguagesturkishindexhtmTrkeTurkish"><span lang="tr">Türçke</span> / Turkish</a></li>
                <li style="height: 20px;"><a href="languages/ukrainian/index.htm" id="footerContainerhttpwwwhsegovuklanguagesukrainianindexhtmUkrainian"><span lang="uk">Українська</span> / Ukrainian</a></li>
                <li style="height: 20px;"><a href="languages/urdu/index.htm" id="footerContainerhttpwwwhsegovuklanguagesurduindexhtmUrdu"><span id="lUrdu" class="nonLatin"></span> / Urdu</a></li>
                <li style="height: 20px;"><a href="welsh/index.htm" id="footerContainerhttpwwwhsegovukwelshindexhtmCymraegWelshleaflets"><span lang="cy">Cymraeg</span> / Welsh<span class="hide"> leaflets</span></a></li>
            </ul></div>
        </div>
    </div>
</div>

<div id="lastUpdated"><!-- InstanceBeginEditable name="updated" -->Updated: <!-- #BeginDate format:IS1 -->2015-07-07<!-- #EndDate --><!-- InstanceEndEditable --></div>
<script src="assets/v4-js/jquery.min.js"></script>
<script src="assets/v4-js/hse-primary.min.js"></script>
<script src="assets/v4-js/core.min.js"></script>


<script src="http://www.google.co.uk/jsapi"></script>
<script src="assets/v4-js/website.min.js"></script><script src="http://www.google.com/uds/?file=search&amp;v=1&amp;output=nocss%3Dtrue" type="text/javascript"></script><script src="http://www.google.com/uds/api/search/1.0/cb6ef4de1f03dde8c26c6d526f8a1f35/default+en.I.js" type="text/javascript"></script>



<div id="gutter"><!-- InstanceBeginEditable name="gutter" -->


<script src="assets/v4-js/extra-homepage.js"></script>
<script> 
	$(window).load(function () {
		$('input#gsc-i-id1').attr('onclick','return SiTrackLink(this)');
		$('input.gsc-search-button').attr('id','gsc-search-button');
		$('input.gsc-search-button').attr('onclick','return SiTrackLink(this)');
	 });
</script>

<!-- InstanceEndEditable --></div>


<!-- InstanceParam name="bodyClass" type="text" value="home" -->
<!-- InstanceParam name="columnLayout" type="number" value="1" -->
<!-- InstanceParam name="intranetDetails" type="boolean" value="true" -->
<!-- InstanceParam name="intranetOGStatus" type="text" value="#" -->
<!-- InstanceParam name="intranetName" type="text" value="#" -->
<!-- InstanceParam name="intranetVPN" type="text" value="#" -->
<!-- InstanceParam name="platform" type="text" value="website" -->
<!-- InstanceParam name="rdfa" type="boolean" value="false" -->
<!-- InstanceParam name="socialMedia" type="boolean" value="true" -->
<!-- InstanceParam name="welsh" type="boolean" value="false" -->
<!-- InstanceParam name="homepage" type="boolean" value="true" -->
<!-- InstanceParam name="abc" type="boolean" value="false" -->
<!-- InstanceParam name="weblite" type="boolean" value="false" -->
<script type="text/javascript">
/*<![CDATA[*/
(function() {
var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
sz.src = '//uk1.siteimprove.com/js/siteanalyze_6798.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
})();
/*]]>*/
</script>

<!-- InstanceEnd --><iframe id="frame-onFontResize1436526145000" title="Ignore this frame" src="javascript:false;" class="div-onfontresize" style="width: 100em; height: 10px; position: absolute; border-width: 0px; top: -5000px; left: -5000px;"></iframe><table cellspacing="0" cellpadding="0" class="gstl_50 gssb_c" style="width: 171px; display: none; top: 30px; left: 699px; position: absolute;"><tbody><tr><td class="gssb_f"></td><td class="gssb_e" style="width: 100%;"></td></tr></tbody></table></body></html>

